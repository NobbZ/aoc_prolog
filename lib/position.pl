%% -*- mode: prolog -*-

:- module(position, []).

old_dir_new(X-Y0, north, X-Y) :- Y is Y0 + 1.
old_dir_new(X-Y0, south, X-Y) :- Y is Y0 - 1.
old_dir_new(X0-Y, west, X-Y) :- X is X0 + 1.
old_dir_new(X0-Y, east, X-Y) :- X is X0 - 1.
