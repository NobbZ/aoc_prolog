PL_FILES = $(sort $(shell find . -name '*.pl'))

all:
	swipl $(PL_FILES:%=-l %) -g run_tests,halt -t 'halt(1)'

run:
	swipl main.pl -- --verbose
