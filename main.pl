%% -*- mode: prolog -*-

:- module(main, []).

:- initialization(main, main).

:- use_module(library(optparse)).
:- use_module(library(option)).

main(Argv) :-
    OptSpec = [
        [opt(verbose), shortflags([v]), longflags([verbose]), type(boolean), default(false), help('Whether to have verbose output or not')],
        [opt(spoil), shortflags([s]), longflags([spoil]), type(boolean), default(false), help('Whether to show results of computations')],
        [opt(help), shortflags([h]), longflags([help]), type(boolean), default(false), help('Print this helptext')]
    ],
    opt_parse(OptSpec, Argv, Opts0, []),
    select_option(help(Help), Opts0, Opts1, help(false)),
    select_option(spoil(Spoil), Opts1, Opts2, spoil(false)),
    select_option(verbose(Verbose), Opts2, [], verbose(false)),
    (Help == true ->
         print_help(OptSpec);
     ModuleSpecs = [
         [15,[1,2,3,4,5]]
     ],
     load_modules(ModuleSpecs, Modules, Verbose),
     run_modules(Modules, Spoil)).

print_help(OptSpec) :-
    opt_help(OptSpec, HelpText),
    write(HelpText).

load_modules([], [], _).
load_modules([[_, []]|Specs], Modules, Verbose) :- load_modules(Specs, Modules, Verbose).
load_modules([[Y,[D|Ds]]|Specs], [Module|Modules0], Verbose) :-
    format(atom(Module), "y~|~`0t~d~2+_d~|~`0t~d~2+", [Y, D]),
    format(atom(Path), "y~w/~w", [Y, Module]),
    statistics(walltime, _),
    (Verbose == true ->
         format("Loading ~w from ~w ", [Module, Path]),
         flush_output;
     true),
    load_files(Path, [imports([]), optimise(true)]),
    statistics(walltime, [_, Duration]),
    (Verbose == true ->
         format("in ~p ms~n", [Duration]);
     true),
    load_modules([[Y, Ds]|Specs], Modules0, Verbose).

run_modules([], _).
run_modules([M|Ms], Spoil) :-
    measure(M:part1, Spoil, _),
    measure(M:part2, Spoil, _),
    run_modules(Ms, Spoil).

measure(P, true, Result) :-
    measure(P, false, Result),
    format("~w >> ~p~n", [P, Result]).

measure(P, false, Solution) :-
    format("~w -> ", [P]),
    flush_output,
    statistics(walltime, _),
    call(P, Hash, Solution),
    statistics(walltime, [_, Duration]),
    format("~|~t~p~8+ in ~|~t~p~5+ ms~n", [Hash, Duration]).
