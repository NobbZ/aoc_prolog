%% -*- mode: prolog -*-

:- module(y15_d05, []).

:- use_module(library(dcg/basics)).
:- use_module(library(dcg/high_order)).

%% Puzzle solvers
%% ==============

part1(Hash, Solution) :-
    part1_('inputs/y15/d05.txt', Solution),
    term_hash(Solution, Hash).

part1_(Filename, Solution) :-
    phrase_from_file(sequence(string, "\n", Words0), Filename),
    include(has_at_least_3_vowels, Words0, Words1),
    include(has_a_letter_twice_in_a_row, Words1, Words2),
    exclude(has_bad_pair, Words2, Words),
    length(Words, Solution), !.

part2(Hash, Solution) :-
    part2_('inputs/y15/d05.txt', Solution),
    term_hash(Solution, Hash).

part2_(Filename, Solution) :-
    phrase_from_file(sequence(string, "\n", Words0), Filename),
    include(none_overlapping_pairs, Words0, Words1),
    include(one_letter_repeating_with_gap, Words1, Words),
    length(Words, Solution), !.

%% Part 1 helpers
%% ==============

has_at_least_3_vowels(Word) :-
    has_at_least_3_vowels_(Word, 0).

has_at_least_3_vowels_(_, 3).
has_at_least_3_vowels_([Ch|W], Cnt0) :-
    (member(Ch, `aeiou`) ->
         Cnt is Cnt0 + 1;
     Cnt is Cnt0),
    has_at_least_3_vowels_(W, Cnt), !.

has_a_letter_twice_in_a_row([A, A|_]) :- !.
has_a_letter_twice_in_a_row([_|T]) :-
    has_a_letter_twice_in_a_row(T).

has_bad_pair([A,B|W]) :-
    member([A,B], [`ab`, `cd`, `pq`, `xy`]);
    has_bad_pair([B|W]).

%% Part 2 helpers
%% ==============

none_overlapping_pairs(Word) :-
    none_overlapping_pairs_(Word, []).

none_overlapping_pairs_([A,B,C|Word], Set0) :-
    \+ ord_memberchk([A,B] ,Set0),
    ord_add_element(Set0, [A,B], Set),
    ((A =:= B, B =:= C) ->
         none_overlapping_pairs_([C|Word], Set);
     none_overlapping_pairs_([B,C|Word], Set)), !.
none_overlapping_pairs_([A, B|_], Set) :-
    ord_memberchk([A, B], Set).

one_letter_repeating_with_gap([A,_,A|_]) :- !.
one_letter_repeating_with_gap([_|T]) :- one_letter_repeating_with_gap(T).

%% Tests
%% =====

:- begin_tests(y15d05p1).

has_at_least_3_vowels_test_data(`aei`).
has_at_least_3_vowels_test_data(`xazegov`).
has_at_least_3_vowels_test_data(`aeiouaeiouaeiou`).

test(valid_3_vowel, [forall(has_at_least_3_vowels_test_data(Input)), true]) :-
    has_at_least_3_vowels(Input).

test(solution, [true(Hash == 12330833)]) :-
    part1(Hash, _Solution).

:- end_tests(y15d05p1).

:- begin_tests(y15d05p2).

none_overlapping_pairs_test_data(`xyxy`).
none_overlapping_pairs_test_data(`aabcdefgaa`).
none_overlapping_pairs_test_data(`xilodxfuxphuiiii`).
none_overlapping_pairs_test_data(`jiyahihykjjkdaya`).
none_overlapping_pairs_test_data(`qmqaqsajmqwhetpk`).
none_overlapping_pairs_test_data(`uedtpzxyubeveuek`).
none_overlapping_pairs_test_data(`lotxrswlxbxlxufs`).

test(valid_none_overlapping_pairs, [forall(none_overlapping_pairs_test_data(Input)), true]) :-
    none_overlapping_pairs(Input).

none_overlapping_pairs_faling_test_data(`aaa`).
none_overlapping_pairs_faling_test_data(`ieodomkazucvgmuy`).

test(invalid_none_overlapping_pairs, [forall(none_overlapping_pairs_faling_test_data(Input)), fail]) :-
    none_overlapping_pairs(Input).

one_letter_repeating_with_gap_test_data(`xyx`).
one_letter_repeating_with_gap_test_data(`abcdefeghi`).
one_letter_repeating_with_gap_test_data(`aaa`).
one_letter_repeating_with_gap_test_data(`qjhvhtzxzqqjkmpb`).
one_letter_repeating_with_gap_test_data(`xxyxx`).
one_letter_repeating_with_gap_test_data(`ieodomkazucvgmuy`).

test(valid_one_letter_with_gap, [forall(one_letter_repeating_with_gap_test_data(Input)), true]) :-
    one_letter_repeating_with_gap(Input).

one_letter_repeating_with_gap_failing_test_data(`uurcxstgmygtbstg`).

test(invalid_one_letter_with_gap, [forall(one_letter_repeating_with_gap_failing_test_data(Input)), fail]) :-
    one_letter_repeating_with_gap(Input).

test(solution, [true(Hash == 13800300)]) :-
    part2(Hash, _Solution).

:- end_tests(y15d05p2).
