/* -*- mode: prolog -*- */

:- module(y15_d01, []).

/* Puzzle solvers */
/* ============== */

part1(Hash, Solution) :-
    part1_('inputs/y15/d01.txt', Solution),
    term_hash(Solution, Hash).

part1_(Filename, Solution) :-
    phrase_from_file(parens(Ps), Filename),
    parens_floor_(Ps, Solution).

part2(Hash, Solution) :-
    part2_('inputs/y15/d01.txt', Solution),
    term_hash(Solution, Hash).

part2_(Filename, Solution) :-
    phrase_from_file(parens(Ps), Filename),
    parens_steps_(Ps, Solution, 0).

/* Part 1 Helpers */
/* ============== */

parens_floor(String, Floor) :-
    phrase(parens(Parens), String),
    parens_floor_(Parens, Floor).

parens_floor_([], 0).
parens_floor_([open|T], Floor) :-
    parens_floor_(T, Floor0),
    Floor is Floor0 + 1.
parens_floor_([close|T], Floor) :-
    parens_floor_(T, Floor0),
    Floor is Floor0 - 1.

/* Part 2 Helpers */
/* ============== */

parens_steps(String, Steps) :-
    phrase(parens(Parens), String),
    parens_steps_(Parens, Steps, 0).

parens_steps_(_, 0, -1).
parens_steps_([open|T], Steps, Floor) :-
    Floor0 is Floor + 1,
    parens_steps_(T, Steps0, Floor0),
    Steps is Steps0 + 1, !.
parens_steps_([close|T], Steps, Floor) :-
    Floor0 is Floor - 1,
    parens_steps_(T, Steps0, Floor0),
    Steps is Steps0 + 1, !.

/* Shared Parsers */
/* ============== */

parens([open|T]) --> `(`, parens(T), !.
parens([close|T]) --> `)`, parens(T), !.
parens(L) --> [_], parens(L), !.
parens([]) --> "".

/* Tests */
/* ===== */

:- begin_tests(y15d01p1).

parens_floor_test_data(`()()`, 0).
parens_floor_test_data(`(())`, 0).
parens_floor_test_data(`(((`, 3).
parens_floor_test_data(`(()(()(`, 3).
parens_floor_test_data(`))(((((`, 3).
parens_floor_test_data(`())`, -1).
parens_floor_test_data(`))(`, -1).
parens_floor_test_data(`)))`, -3).
parens_floor_test_data(`)())())`, -3).

test(examples, [forall(parens_floor_test_data(Input, Expected))]) :-
    parens_floor(Input, Result),
    assertion(Result = Expected).

test(solution, [true(Hash == 9760350)]) :-
    part1(Hash, _Solution).

:- end_tests(y15d01p1).

:- begin_tests(y15d01p2).

parens_steps_test_data(`)`, 1).
parens_steps_test_data(`()())`, 5).

test(examples, [forall(parens_steps_test_data(Input, Expected))]) :-
    parens_steps(Input, Result),
    assertion(Result = Expected).

test(solution, [true(Hash = 1463377)]) :-
    part2(Hash, _Solution).

:- end_tests(y15d01p2).
