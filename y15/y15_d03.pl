/* -*- mode: prolog -*- */

:- module(y15_d03, []).

:- use_module('../lib/position', []).

%% Puzzle solvers
%% ==============

part1(Hash, Solution) :-
    part1_('inputs/y15/d03.txt', Solution),
    term_hash(Solution, Hash).

part1_(Filename, Solution) :-
    phrase_from_file(moves(Ms), Filename),
    moves_santa_(Ms, Solution).

part2(Hash, Solution) :-
    part2_('inputs/y15/d03.txt', Solution),
    term_hash(Solution, Hash).

part2_(Filename, Solution) :-
    phrase_from_file(moves(Ms), Filename),
    moves_santa_robot_(Ms, Solution).

%% Part 1 Helpers
%% ==============

moves_santa(Moves, Santa) :-
    phrase(moves(Ms), Moves),
    moves_santa_(Ms, Santa).

moves_santa_(Moves, Santa) :-
    moves_santa__(Moves, [0-0], 0-0, HousesSet),
    length(HousesSet, Santa).

moves_santa__([], SantaPos, _, SantaPos) :- !.
moves_santa__([D|Ds], Ps0, Pos0, HousesSet) :-
    position:old_dir_new(Pos0, D, Pos),
    ord_add_element(Ps0, Pos, Ps),
    moves_santa__(Ds, Ps, Pos, HousesSet), !.

%% Part 2 Helpers
%% ==============

moves_santa_robot(Moves, Solution) :-
    phrase(moves(Ms), Moves),
    moves_santa_robot_(Ms, Solution).

moves_santa_robot_(Moves, Solution) :-
    moves_santa_robot__(Moves, 0-0, 0-0, [0-0], Houses),
    length(Houses, Solution).

moves_santa_robot__([], _, _, Houses, Houses).
moves_santa_robot__([DS, DR|T], CS0, CR0, HS0, Houses) :-
    position:old_dir_new(CS0, DS, CS), position:old_dir_new(CR0, DR, CR),
    ord_add_element(HS0, CS, HS1),
    ord_add_element(HS1, CR, HS),
    moves_santa_robot__(T, CS, CR, HS, Houses).

%% Shared Parsers
%% ==============

moves([north|T]) --> `^`, moves(T), !.
moves([west|T]) --> `>`, moves(T), !.
moves([south|T]) --> `v`, moves(T), !.
moves([east|T]) --> `<`, moves(T), !.
moves(T) --> [_], moves(T), !.
moves([]) --> [].

%% Tests
%% =====

:- begin_tests(y15d03p1).

moves_santa_test_data(`>`, 2).
moves_santa_test_data(`^>v<`, 4).
moves_santa_test_data(`^v^v^v^v^v`, 2).

test(examples, [forall(moves_santa_test_data(Input, Expected))]) :-
    moves_santa(Input, Result),
    assertion(Result == Expected).

test(solution, [true(Hash == 5253570)]) :-
    part1(Hash, _Solution).

:- end_tests(y15d03p1).

:- begin_tests(y15d03p2).

moves_santa_robot_test_data(`^v`, 3).
moves_santa_robot_test_data(`^>v<`, 3).
moves_santa_robot_test_data(`^v^v^v^v^v`, 11).

test(examples, [forall(moves_santa_robot_test_data(Input, Expected)), true(Result == Expected)]) :-
    moves_santa_robot(Input, Result).

test(solution, [true(Hash == 8392565)]) :-
    part2(Hash, _Solution).

:- end_tests(y15d03p2).
