/* -*- mode: prolog -*- */

:- module(y15_d02, []).

:- use_module(library(clpfd)).

:- use_module(library(dcg/basics)).

:- expects_dialect(sicstus).

/* Puzzle solvers */
/* ============== */

part1(Hash, Solution) :-
    part1_('inputs/y15/d02.txt', Solution),
    term_hash(Solution, Hash).

part1_(Filename, Solution) :-
    phrase_from_file(boxes(Bs), Filename),
    boxes_paper_(Bs, Solution).

part2(Hash, Solution) :-
    part2_('inputs/y15/d02.txt', Solution),
    term_hash(Solution, Hash).

part2_(Filename, Solution) :-
    phrase_from_file(boxes(Bs), Filename),
    boxes_ribbon_(Bs, Solution).

/* Part 1 Helpers */
/* ============== */

boxes_paper(Boxes, Paper) :-
    phrase(boxes(Bs), Boxes),
    boxes_paper_(Bs, Paper).

boxes_paper_([], 0).
boxes_paper_([box(L,W,H)|Bs], Paper) :-
    msort([L,W,H], [A,B,_]),
    Paper #= Paper0 + 2*L*W + 2*W*H + 2*H*L + A*B,
    boxes_paper_(Bs, Paper0).

/* Part 2 Helpers */
/* ============== */

boxes_ribbon(Boxes, Ribbon) :-
    phrase(boxes(Bs), Boxes),
    boxes_ribbon_(Bs, Ribbon).

boxes_ribbon_([], 0).
boxes_ribbon_([box(L,W,H)|Bs], Ribbon) :-
    msort([L,W,H], [A,B,_]),
    Ribbon #= Ribbon0 + 2*A + 2*B + L*W*H,
    boxes_ribbon_(Bs, Ribbon0).

/* Shared Parsers */
/* ============== */

box(L, W, H) --> integer(L), `x`, integer(W), `x`, integer(H).

boxes([box(L,W,H)|T]) -->
    box(L, W, H),
    `\n`,
    boxes(T), !.
boxes([box(L,W,H)]) -->
    box(L,W,H), !.
boxes([]) --> [].

/* Tests */
/* ===== */

:- begin_tests(y15d02parse).

present_parser_test_data(`2x3x4`, box(2,3,4)).
present_parser_test_data(`1x1x10`, box(1,1,10)).

test(parse_single_box, [forall(present_parser_test_data(Input, Expected))]) :-
    phrase(box(L,W,H), Input),
    assertion(box(L,W,H) == Expected).

test(parse_boxes) :-
    phrase(boxes(Bs), `2x3x4\n1x1x10`),
    assertion([box(2,3,4), box(1,1,10)] == Bs).

:- end_tests(y15d02parse).

:- begin_tests(y15d02p1).

present_wrap_test_data(`2x3x4`, 58).
present_wrap_test_data(`1x1x10`, 43).
present_wrap_test_data(`2x3x4\n1x1x10`, 101).

test(example, [forall(present_wrap_test_data(Input, Expected))]) :-
    boxes_paper(Input, Result),
    assertion(Result == Expected).

test(solution, [true(Hash = 7793588)]) :-
    part1(Hash, _Solution).

:- end_tests(y15d02p1).

:- begin_tests(y15d02p2).

present_ribbon_test_data(`2x3x4`, 34).
present_ribbon_test_data(`1x1x10`, 14).
present_ribbon_test_data(`2x3x4\n1x1x10`, 48).

test(examples, [forall(present_ribbon_test_data(Input, Expected))]) :-
    boxes_ribbon(Input, Result),
    assertion(Result = Expected).

test(solution, [true(Hash = 12027876)]) :-
    part2(Hash, _Solution).

:- end_tests(y15d02p2).
