/* -*- mode: prolog -*- */

:- module(y15_d04, []).

:- use_module(library(lists)).
:- use_module(library(md5)).

%% Puzzle solvers
%% ==============

part1(Hash, Solution) :-
    part1_(yzbqklnj, Solution),
    term_hash(Solution, Hash).

part1_(Input, Solution) :-
    find_hash_(Input, 1, Solution).

part2(Hash, Solution) :-
    part2_(yzbqklnj, Solution),
    term_hash(Solution, Hash).

part2_(Input, Solution) :-
    find_hash2_(Input, 1, Solution).

%% Part 1 Helpers
%% ==============

find_hash_(Input, N0, Solution) :-
    format(codes(List), "~w~d", [Input, N0]),
    md5_hash(List, Hash, []),
    (sub_atom(Hash, 0, _, _, '00000') ->
         Solution is N0;
     (N is N0 + 1,
      find_hash_(Input, N, Solution))).

%% Part 2 Helpers
%% ==============

find_hash2_(Input, N0, Solution) :-
    format(codes(List), "~w~d", [Input, N0]),
    md5_hash(List, Hash, []),
    (sub_atom(Hash, 0, _, _, '000000') ->
         Solution is N0;
     (N is N0 + 1,
      find_hash2_(Input, N, Solution))).

%% Tests
%% =====

:- begin_tests(y15d04p1).

part1_test_data('abcdef', 609043).
part1_test_data('pqrstuv', 1048970).

test(examples, [forall(part1_test_data(Input, Expected)),
        true(Result == Expected)]) :-
    part1_(Input, Result).

test(solution, [true(Hash == 5989212)]) :-
    part1_('yzbqklnj', Solution),
    term_hash(Solution, Hash).

:- end_tests(y15d04p1).

:- begin_tests(y15d04p2).

test(solution, [true(Hash == 9691604)]) :-
    part2_('yzbqklnj', Solution),
    term_hash(Solution, Hash).

:- end_tests(y15d04p2).
